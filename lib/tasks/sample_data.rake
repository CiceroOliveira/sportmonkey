namespace :db do
  desc 'Fill the database with sample data'
  task :populate => :environment do
    Rake::Task['db:reset'].invoke
    Golfer.create!(:name => 'Example Golfer')
    99.times do |n|
      name = Faker::Name.name
      Golfer.create!(:name => name)
    end
  end
end