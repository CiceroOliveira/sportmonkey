class CreateResults < ActiveRecord::Migration
  def change
    create_table :results do |t|
      t.integer :tournament_id
      t.integer :golfer_id
      t.integer :place
      t.decimal :winnings, :precision => 10, :scale => 2

      t.timestamps
    end
  end
end
