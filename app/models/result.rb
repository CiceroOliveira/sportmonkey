class Result < ActiveRecord::Base
  belongs_to :tournament
  belongs_to :golfer
  
  validates :place, :winnings, :tournament_id, :golfer_id, presence: true
end
