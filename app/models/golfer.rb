class Golfer < ActiveRecord::Base
  has_many :results
  has_many :tournaments, :through => :results
  
  scope :initial, lambda {|alphabet| where("name LIKE ?", "#{alphabet}%") }
end
