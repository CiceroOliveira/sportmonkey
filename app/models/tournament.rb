class Tournament < ActiveRecord::Base
  has_many :results
  has_many :golfers, :through => :results
end
