class GolfersController < ApplicationController
  def list
    @title = "Golfers List"
    @golfers = Golfer.initial(params[:alphabet])
    
    render 'index'
  end
  
  # GET /golfers
  # GET /golfers.json
  def index
    @title = "Golfers"
    @golfers = Golfer.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @golfers }
    end
  end

  # GET /golfers/1
  # GET /golfers/1.json
  def show
    @golfer = Golfer.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @golfer }
    end
  end
end
