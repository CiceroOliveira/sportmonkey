class PagesController < ApplicationController
  layout "landing_page"
  
  def home
    @title = 'Welcome'
  end

  def contact
    @title = 'Contact'
  end

  def about
    @title = 'About'
  end

end
