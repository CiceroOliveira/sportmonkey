require 'spec_helper'

describe Golfer do  
  describe 'get golfers by first letter' do
    it 'should return by letter' do
      Golfer.create!(:name  => "A Golfer")
      Golfer.create!(:name  => "A Golfer 2")
      Golfer.create!(:name  => "B Golfer")
      
      golfers_a = Golfer.initial("A")
      golfers_a.count.should == 2
    end
  end
end
