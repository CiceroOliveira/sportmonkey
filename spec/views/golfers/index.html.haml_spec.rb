require 'spec_helper'

describe "golfers/index" do
  before(:each) do
    assign(:golfers, [
      stub_model(Golfer,
        :name => "Golfer"
      ),
      stub_model(Golfer,
        :name => "Golfer"
      )
    ])
  end

  it "renders a list of golfers" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Golfer".to_s, :count => 2
  end
end
