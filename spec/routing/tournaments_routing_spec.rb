require "spec_helper"

describe TournamentsController do
  describe "routing" do

    it "routes to #index" do
      get("/tournaments").should route_to("tournaments#index")
    end

    it "routes to #show" do
      get("/tournaments/1").should route_to("tournaments#show", :id => "1")
    end

  end
end
