require "spec_helper"

describe GolfersController do
  describe "routing" do

    it "routes to #index" do
      get("/golfers").should route_to("golfers#index")
    end

    it "routes to #show" do
      get("/golfers/1").should route_to("golfers#show", :id => "1")
    end
  end
end
